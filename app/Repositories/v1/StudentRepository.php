<?php
/**
 * @file  StudentRepository.php
 * StudentRepository Repository
 * @author  Chathura Fernando
 */

namespace App\Repositories\v1;

use App\Models\Classes;
use App\Models\Student;
use App\Repositories\Contracts\v1\StudentRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class StudentRepository implements StudentRepositoryInterface
{

    private $student;

    /**
     * @param Student $student
     */
    public function __construct(
        Student $student
    ) {
        $this->student = $student;
    }

    /**
     * List down all the grade details
     *
     * @return object
     */
    public function getStudentList()
    {
        return $this->student
            ->leftJoin('trn_classes', 'trn_classes.id', '=', 'trn_student.class_id')
            ->leftJoin('trn_grades', 'trn_grades.id', '=', 'trn_classes.grade_id')
            ->select(
            'trn_student.id',
            'first_name',
            'last_name',
            'trn_classes.name as class',
            'trn_grades.name as grade'
        )->orderBy('trn_student.id', 'DESC')->paginate(10);
    }

    /**
     * Create new grade
     *
     * @param $data
     * @return static
     */
    public function create($data)
    {
        return $this->student->insert($data);
    }
}
