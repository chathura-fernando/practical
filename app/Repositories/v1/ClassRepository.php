<?php
/**
 * @file  ClassRepository.php
 * ClassRepository Repository
 * @author  Chathura Fernando
 */

namespace App\Repositories\v1;

use App\Models\Classes;
use App\Repositories\Contracts\v1\ClassRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class ClassRepository implements ClassRepositoryInterface
{

    private $class;

    /**
     * @param Classes $class
     */
    public function __construct(
        Classes $class
    ) {
        $this->class = $class;
    }

    /**
     * List down all the grade details
     *
     * @return object
     */
    public function getClassList()
    {
        return $this->class->leftJoin('trn_grades', 'trn_grades.id', '=', 'trn_classes.grade_id')->select(
            'trn_classes.id',
            'trn_classes.name',
            'grade_id',
            'trn_grades.name as grade'
        )->orderBy('trn_classes.id', 'DESC')->paginate(10);
    }

    /**
     * Create new grade
     *
     * @param $data
     * @return object
     */
    public function create($data)
    {
        return $this->class->insert($data);
    }

    /**
     * Get class list for exact grade
     *
     * @param $gradeId
     * @return object
     */
    public function getClassListByGrade($gradeId)
    {
        return $this->class->select(
            'id',
            'name'
        )->orderBy('name', 'ASC')
        ->where('grade_id', $gradeId)
        ->get();
    }

    /**
     * Check is the class can delete
     *
     * @param $classId
     * @return bool
     */
    public function allowDelete($classId)
    {
        $count = DB::table('trn_student')->select('id')->where('class_id', $classId)->count();
        return ($count == 0)? true : false;
    }

    /**
     * Delete class
     *
     * @param $classId
     * @return bool
     */
    public function delete($classId)
    {
        $this->class->find($classId)->delete();
        return true;
    }



}
