<?php
/**
 * @file  GradeRepository.php
 * GradeRepository Repository
 * @author  Chathura Fernando
 */

namespace App\Repositories\v1;

use App\Models\Grade;
use App\Repositories\Contracts\v1\GradeRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class GradeRepository implements GradeRepositoryInterface
{

    private $grade;

    /**
     * @param Grade $grade
     */
    public function __construct(
        Grade $grade
    ) {
        $this->grade = $grade;
    }

    /**
     * List down all the grade details
     *
     * @param bool|true $paginate
     * @return object
     */
    public function getGradeList($paginate = true)
    {
        $data =  $this->grade->select(
            'id',
            'name'
        );

        if ($paginate) {
            return $data->orderBy('id', 'DESC')->paginate(10);
        } else {
            return $data->orderBy('name', 'ASC')->get();
        }
    }

    /**
     * Create new grade
     *
     * @param $data
     * @return static
     */
    public function create($data)
    {
        return $this->grade->insert($data);
    }
}
