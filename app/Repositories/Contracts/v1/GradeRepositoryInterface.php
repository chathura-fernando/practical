<?php
/**
 * @file  GradeRepositoryInterface.php
 * GradeRepository Interface
 * @author  Chathura Fernando
 */
 
namespace App\Repositories\Contracts\v1;
 
interface GradeRepositoryInterface
{
    /**
     * List down all the grade details
     *
     * @param bool|true $paginate
     * @return object
     */
    public function getGradeList($paginate = true);

    /**
     * Create new grade
     *
     * @param $data
     * @return static
     */
    public function create($data);

}
