<?php
/**
 * @file  StudentRepositoryInterface.php
 * StudentRepositoryInterface Interface
 * @author  Chathura Fernando
 */
 
namespace App\Repositories\Contracts\v1;
 
interface StudentRepositoryInterface
{
    /**
     * List down all the grade details
     *
     * @return object
     */
    public function getStudentList();

    /**
     * Create new grade
     *
     * @param $data
     * @return static
     */
    public function create($data);
}
