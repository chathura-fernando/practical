<?php
/**
 * @file  ClassRepositoryInterface.php
 * ClassRepository Interface
 * @author  Chathura Fernando
 */
 
namespace App\Repositories\Contracts\v1;
 
interface ClassRepositoryInterface
{
    /**
     * List down all the grade details
     *
     * @return object
     */
    public function getClassList();

    /**
     * Create new grade
     *
     * @param $data
     * @return object
     */
    public function create($data);

    /**
     * Get class list for exact grade
     *
     * @param $gradeId
     * @return object
     */
    public function getClassListByGrade($gradeId);

    /**
     * Check is the class can delete
     *
     * @param $classId
     * @return bool
     */
    public function allowDelete($classId);

    /**
     * Delete class
     *
     * @param $classId
     * @return bool
     */
    public function delete($classId);

}
