<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api/v1', 'middleware' => 'tokenValidator'], function () {

        Route::get('grades/get-list', 'GradeController@getGradeList');
        Route::resource('grades', 'GradeController', ['except' => 'destroy']);

        Route::get('classes/get-list-for-grade', 'ClassController@getClassListByGrade');
        Route::resource('classes', 'ClassController');

        Route::resource('students', 'StudentController', ['except' => 'destroy']);
});
