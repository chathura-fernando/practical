<?php
/**
 * @file  GradeController.php
 *  GradeController Controller
 * @author  Chathura Fernando
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\v1\Helper;
use App\Repositories\Contracts\v1\GradeRepositoryInterface;
use Illuminate\Support\Facades\Input;

class GradeController extends Controller
{
    private $gradeRepo;

    /**
     * @param GradeRepositoryInterface $gradeRepo
     * @param Helper $helper
     */
    public function __construct(
        GradeRepositoryInterface $gradeRepo,
        Helper $helper

    ) {
        $this->gradeRepo = $gradeRepo;
        $this->helper = $helper;
    }

    /**
     * Provide data for grade listing
     *
     * @return array
     */
    public function index()
    {
        try {
            $returnValue = $this->gradeRepo->getGradeList(true);
            return $this->helper
                ->response(200, ['data' => $returnValue]
                );
        } catch (\Exception $ex) {

            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }

    /**
     * Save new grade
     *
     * @return array
     */
    public function store()
    {
        try {
            $data = Input::only('name', 'description');
            if ($data['name'] == ''  || $data['name'] == null) {
                return $this->helper
                    ->response(212, ['message' => 'Grade name cannot be empty']
                    );
            }
            $returnValue = $this->gradeRepo->create($data);
            return $this->helper
                ->response(200,
                    ['message' => 'New grade added successfully.', 'data' => $returnValue]
                );
        } catch (\Exception $ex) {
            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }

    /**
     * Get grade list
     *
     * @return array
     */
    public function getGradeList()
    {
        try {
            $returnValue = $this->gradeRepo->getGradeList(false);
            return $this->helper
                ->response(200, ['data' => $returnValue]
                );
        } catch (\Exception $ex) {

            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }
}
