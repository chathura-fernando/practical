<?php
/**
 * @file  StudentController.php
 *  StudentController Controller
 * @author  Chathura Fernando
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\v1\Helper;
use App\Repositories\Contracts\v1\StudentRepositoryInterface;
use Illuminate\Support\Facades\Input;

class StudentController extends Controller
{
    private $gradeRepo;

    /**
     * @param StudentRepositoryInterface $studentRepo
     * @param Helper $helper
     */
    public function __construct(
        StudentRepositoryInterface $studentRepo,
        Helper $helper

    ) {
        $this->studentRepo = $studentRepo;
        $this->helper = $helper;
    }

    /**
     * Provide data for grade listing
     *
     * @return array
     */
    public function index()
    {
        try {
            $returnValue = $this->studentRepo->getStudentList();
            return $this->helper
                ->response(200, ['data' => $returnValue]
                );
        } catch (\Exception $ex) {

            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }

    /**
     * Save new grade
     *
     * @return array
     */
    public function store()
    {
        try {
            $data = Input::only('first_name', 'last_name', 'surname', 'gender', 'class_id');
            if ($data['first_name'] == ''  || $data['first_name'] == null) {
                return $this->helper
                    ->response(212, ['message' => 'First name cannot be empty']
                    );
            }
            if ($data['last_name'] == ''  || $data['last_name'] == null) {
                return $this->helper
                    ->response(212, ['message' => 'Last name cannot be empty']
                    );
            }
            if ($data['surname'] == ''  || $data['surname'] == null) {
                return $this->helper
                    ->response(212, ['message' => 'Surname cannot be empty']
                    );
            }
            if ($data['class_id'] == ''  || $data['class_id'] == null) {
                return $this->helper
                    ->response(212, ['message' => 'Class cannot be empty']
                    );
            }
            $data['gender'] = ($data['gender'] == 1)? 1 : 0;
            $returnValue = $this->studentRepo->create($data);
            return $this->helper
                ->response(200,
                    ['message' => 'New student added successfully.', 'data' => $returnValue]
                );
        } catch (\Exception $ex) {
            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }
}
