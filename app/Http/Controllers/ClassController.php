<?php
/**
 * @file  ClassController.php
 *  ClassController Controller
 * @author  Chathura Fernando
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\v1\Helper;
use App\Repositories\Contracts\v1\ClassRepositoryInterface;
use Illuminate\Support\Facades\Input;

class ClassController extends Controller
{
    private $gradeRepo;

    /**
     * @param ClassRepositoryInterface $classRepo
     * @param Helper $helper
     */
    public function __construct(
        ClassRepositoryInterface $classRepo,
        Helper $helper

    ) {
        $this->classRepo = $classRepo;
        $this->helper = $helper;
    }

    /**
     * Provide data for grade listing
     *
     * @return array
     */
    public function index()
    {
        try {
            $returnValue = $this->classRepo->getClassList();
            return $this->helper
                ->response(200, ['data' => $returnValue]
                );
        } catch (\Exception $ex) {

            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }

    /**
     * Get class list for exact grade
     *
     * @return array
     */
    public function getClassListByGrade()
    {
        try {
            $data = Input::only('grade_id');
            $returnValue = $this->classRepo->getClassListByGrade($data['grade_id']);
            return $this->helper
                ->response(200, ['data' => $returnValue]
                );
        } catch (\Exception $ex) {

            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }

    /**
     * Save new grade
     *
     * @return array
     */
    public function store()
    {
        try {
            $data = Input::only('name', 'description', 'grade_id');
            if ($data['name'] == ''  || $data['name'] == null) {
                return $this->helper
                    ->response(212, ['message' => 'Class name cannot be empty']
                    );
            }
            if ($data['grade_id'] == ''  || $data['grade_id'] == null) {
                return $this->helper
                    ->response(212, ['message' => 'Grade cannot be empty']
                    );
            }
            $returnValue = $this->classRepo->create($data);
            return $this->helper
                ->response(200,
                    ['message' => 'New class added successfully.', 'data' => $returnValue]
                );
        } catch (\Exception $ex) {
            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }

    /**
     * Delete class
     *
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        try {
            if (!$this->classRepo->allowDelete($id)) {
                return $this->helper
                    ->response(212, ['message' => 'Class cannot be deleted, Students available for this class.']
                    );
            }
            $returnValue = $this->classRepo->delete($id);
            return $this->helper
                ->response(200,
                    ['message' => 'Class deleted successfully.', 'data' => $returnValue]
                );
        } catch (\Exception $ex) {
            return $this->helper
                ->response(500, ['message' => $ex->getMessage()]
                );
        }
    }
}
