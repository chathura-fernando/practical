<?php
/**
 * @file  tokenValidator.php
 *  tokenValidator
 * @author  Chathura Fernando
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use App\Libraries\v1\Helper;

class TokenValidator
{
    private $helper;

    /**
     * @param Helper $helper
     */
    public function __construct(
        Helper $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return $this
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->headers->has('Pyxle-auth-token')) {

            if (strlen($request->headers->get('Pyxle-auth-token')) == 8) {
                return $next($request);
            } else {
                return $this->helper->response(422, ['message' => 'Authentication failed']);
            }
        } else {
            return $this->helper->response(422, ['message' => 'Authentication failed']);
        }
    }
}
