<?php
/**
 * @file  Grade.php
 * Grade Model
 * @author  Chathura Fernando
 */

namespace App\Models;

class Grade extends \Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trn_grades';

}
