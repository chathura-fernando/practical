<?php
/**
 * @file  Classes.php
 * Class Model
 * @author  Chathura Fernando
 */

namespace App\Models;

class Classes extends \Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trn_classes';
}
