<?php
/**
 * @file  Student.php
 * Class Model
 * @author  Chathura Fernando
 */

namespace App\Models;

class Student extends \Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trn_student';
}
