<?php
/**
 * @file  Helper.php
 * @author  Chathura Fernando
 *
 */

namespace app\Libraries\v1;

use Illuminate\Http\Response;

class Helper
{
    public function __construct()
    {

    }

    /**
     * Get response
     *
     * @param $status
     * @param $message
     * @return $this
     */
    public function response($status, $message)
    {
        return (new Response($message, $status))
            ->header('Content-Type', 'application/json');
    }

    /**
     * @param $value
     * @param int $decimals
     *
     * @return null|string
     */
    public static function formatToNumber($value, $decimals = 2)
    {
        if (trim($value) != null) {
            return number_format($value, $decimals, '.', '');
        }
        return null;
    }
}
