<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('script_tags_free', function ($attribute, $value, $parameters, $validator) {
            $originalValue = $value;
            $value = strip_tags($value);

            if (strlen($originalValue) == strlen($value)) {
                return true;
            } else {
                return false;
            }
        });

        Blade::extend(function ($value, $compiler) {
            $value = preg_replace('/(\s*)@switch\((.*)\)(?=\s)/', '$1<?php switch($2):', $value);
            $value = preg_replace('/(\s*)@endswitch(?=\s)/', '$1endswitch; ?>', $value);
            $value = preg_replace('/(\s*)@case\((.*)\)(?=\s)/', '$1case $2: ?>', $value);
            $value = preg_replace('/(?<=\s)@default(?=\s)/', 'default: ?>', $value);
            $value = preg_replace('/(?<=\s)@breakswitch(?=\s)/', '<?php break;', $value);
            return $value;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\v1\GradeRepositoryInterface',
            'App\Repositories\v1\GradeRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\v1\ClassRepositoryInterface',
            'App\Repositories\v1\ClassRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\v1\StudentRepositoryInterface',
            'App\Repositories\v1\StudentRepository'
        );
    }
}
