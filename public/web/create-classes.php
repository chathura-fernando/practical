<?php include 'templates/header.php'; ?>

        <div id="page-wrapper" ng-app="myApp" ng-controller="ClassCreateController">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Class</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!--Response message area-->
            <div class="alert alert-danger" ng-show="!gradeForm.$valid" ng-if="gradeForm.$submitted">
                <ul ng-messages="gradeForm.name.$error" ng-if="gradeForm.$submitted">
                    <li ng-message="required" >Class name is required.</li>
                </ul>
                <ul ng-messages="gradeForm.grade.$error" ng-if="gradeForm.$submitted">
                    <li ng-message="required">Grade is required</li>
                </ul>
            </div>
            <!--Response message area end-->

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            New Class
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <form name="gradeForm" novalidate class="form-horizontal" accept-charset="UTF-8" autocomplete="off">
                                <fieldset ng-disabled="isSubmitting">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel panel-white">
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">Name<span class="pr-required-ast">*</span></label>
                                                        <div class="col-md-5">
                                                            <input type="text" name="name" id="name" ng-model="grade.name"
                                                                   required class="form-control"/>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">Grade<span class="pr-required-ast">*</span></label>
                                                        <div class="col-md-5">
                                                            <select class="form-control" required name="grade"
                                                                    ng-model="grade.grade"
                                                                    ng-options="row.id as row.name for row in gradeList">
                                                                <option value="">Select grade</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="description" class="col-md-2 control-label">Description</label>
                                                        <div class="col-md-5">
                                                            <input type="text" name="description" id="description" ng-model="grade.description"
                                                                   class="form-control"/>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="form-group user-top-form-group">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-9 ep-form-buttons pr-dual-for-component dropbutton-create">


                                                            <button class="btn btn-primary bLarge b-create" type="submit"
                                                                    ng-click="createClass(gradeForm.$valid)" ng-disabled="isSubmitting">
                                                                <i class="fa fa-plus"></i> Create
                                                            </button>
                                                            <a href="classes.php">
                                                                <button class="btn btn-success bLarge b-cancel ep-list-view-top-filter-btn" type="button"
                                                                    ><i class="fa fa-times"></i> Cancel
                                                            </button></a>

                                                        </div>
                                                        <div class="col-md-1"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>



                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->


        </div>
        <!-- /#page-wrapper -->
        <script src="custom_js/ClassCreateController.js"></script>

<?php include 'templates/footer.php'; ?>