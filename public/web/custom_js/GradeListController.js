/**
 * @file  GradeListController.js
 * GradeListController Controller
 * @author  Chathura Fernando
 */

var app = angular.module('myApp', []);

app.controller('GradeListController',
    function ($scope, $http, $location) {

        $scope.grades = [];
        $scope.isPreviousPage = false;
        $scope.isNextPage = false;
        $scope.totalPages = 0;
        $scope.paginationText = [];
        $scope.params = {
            page: parseInt($location.search().page ? $location.search().page : 1)
        };

        /**
         * Generate 8 digit random number of authenticate
         *
         * @returns {string}
         */
        $scope.randomNumberGenerator = function () {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
            var string_length = 8;
            var myrnd = [], pos;
            while (string_length--) {
                pos = Math.floor(Math.random() * chars.length);
                myrnd.push(chars.substr(pos, 1));
            }
            return myrnd.join('');
        };

        /**
         * Function to load initial data when the controller load
         */
        var render = function () {
            $scope.isPreviousPage = false;
            $scope.isNextPage = false;
            $http({
                method: "GET",
                url: "http://practical.dev/api/v1/grades",
                params: $location.search(),
                headers: {
                    'Pyxle-auth-token': $scope.randomNumberGenerator(),
                },
            }).then(function mySucces(response) {

                $scope.grades = response.data.data.data;
                if (response.data.data.current_page > 1) {
                    $scope.isPreviousPage = true;
                }
                if (response.data.data.last_page != response.data.data.current_page) {
                    $scope.isNextPage = true;
                }
                if (response.data.data.last_page) {
                    $scope.totalPages = response.data.data.last_page;

                    $scope.paginationText['total'] = response.data.data.total;
                    $scope.paginationText['from'] = response.data.data.from;
                    $scope.paginationText['to'] = response.data.data.to;
                }

            }, function myError(response) {
                alert('An error occurred. Please try again later');
            });
        };
        render();

        /**
         * Pagination helpers
         */
        $scope.loadPreviousResult = function () {
            $location.search('page', (parseInt($location.search().page) || 0) - 1);
            $scope.params.page = parseInt($location.search().page ? $location.search().page : 1);
            render();
        };

        /**
         * Pagination helpers
         */
        $scope.loadNextResult = function () {
            $location.search('page', (parseInt($location.search().page) || 1) + 1);
            $scope.params.page = parseInt($location.search().page ? $location.search().page : 1);
            render();
        };

        /**
         * Pagination helpers
         */
        $scope.jumpToPage = function () {
            var pageNo = parseInt($scope.params.page);

            if (!isNaN(pageNo) && pageNo > 0 && pageNo <= $scope.totalPages) {
                $location.search('page', pageNo);
            } else if (pageNo <= 0 || pageNo > $scope.totalPages) {
                alert('invalid page');
            }
            $scope.params.page = parseInt($location.search().page ? $location.search().page : 1);
            render();
        };
    });


