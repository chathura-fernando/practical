/**
 * @file  ClassCreateController.js
 * ClassCreateController Controller
 * @author  Chathura Fernando
 */

var app = angular.module('myApp', []);

app.controller('ClassCreateController',
    function ($scope, $http, $location) {

        $scope.isLoading = true;
        $scope.isSubmitting = false;
        $scope.errorMessage = '';
        $scope.grade = {};
        $scope.successRedirect = false;
        $scope.helpText = '';
        $scope.gradeList = {};

        /**
         * Generate 8 digit random number of authenticate
         *
         * @returns {string}
         */
        $scope.randomNumberGenerator = function () {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
            var string_length = 8;
            var myrnd = [], pos;
            while (string_length--) {
                pos = Math.floor(Math.random() * chars.length);
                myrnd.push(chars.substr(pos, 1));
            }
            return myrnd.join('');
        };

        /**
         * Function to load initial data when the controller load
         */
        var render = function () {
            $http({
                method: "GET",
                url: "http://practical.dev/api/v1/grades/get-list",
                headers: {
                    'Pyxle-auth-token': $scope.randomNumberGenerator(),
                },
            }).then(function mySucces(response) {

                $scope.isSubmitting = false;
                if (response && response.status && response.status === 200) {
                    $scope.gradeList = response.data.data;
                }
            }, function myError(response) {
            });
        };
        render();

        /**
         * Create new class
         *
         * @param isValid
         * @param returnType
         */
        $scope.createClass = function (isValid, returnType) {
            if(isValid){
                $scope.isSubmitting = true;

                $http({
                    method: "POST",
                    url: "http://practical.dev/api/v1/classes",
                    data: {
                        name: $scope.grade.name,
                        description: $scope.grade.description,
                        grade_id: $scope.grade.grade
                    },
                    headers: {
                        'Pyxle-auth-token': $scope.randomNumberGenerator(),
                    },
                }).then(function mySucces(response) {

                    $scope.isSubmitting = false;
                    if (response && response.status && response.status === 200) {
                        alert('New class added successfully.');
                        window.location = 'classes.php';
                    } else {
                        alert(response.data.message);
                    }

                }, function myError(response) {
                    $scope.isSubmitting = false;
                    alert('Unexpected error occurred. Please try again later.');
                });
            }
        };

    });