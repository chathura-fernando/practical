/**
 * @file  StudentCreateController.js
 * StudentCreateController Controller
 * @author  Chathura Fernando
 */

var app = angular.module('myApp', []);

app.controller('StudentCreateController',
    function ($scope, $http, $location) {

        $scope.isLoading = true;
        $scope.isSubmitting = false;
        $scope.errorMessage = '';
        $scope.grade = {}
        $scope.successRedirect = false;
        $scope.helpText = '';
        $scope.gradeList = {};
        $scope.classList = {};
        $scope.classText = '';

        /**
         * Generate 8 digit random number of authenticate
         *
         * @returns {string}
         */
        $scope.randomNumberGenerator = function () {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
            var string_length = 8;
            var myrnd = [], pos;
            while (string_length--) {
                pos = Math.floor(Math.random() * chars.length);
                myrnd.push(chars.substr(pos, 1));
            }
            return myrnd.join('');
        };

        /**
         * Function to load initial data when the controller load
         */
        var render = function () {
            $http({
                method: "GET",
                url: "http://practical.dev/api/v1/grades/get-list",
                headers: {
                    'Pyxle-auth-token': $scope.randomNumberGenerator(),
                },
            }).then(function mySucces(response) {
                if (response && response.status && response.status === 200) {
                    $scope.gradeList = response.data.data;
                }
            }, function myError(response) {
            });
        };
        render();

        /**
         * List classes for exact grade
         */
        $scope.getClassList = function () {
            $scope.isSubmitting = true;
            $scope.classText = '';
            $http({
                method: "GET",
                url: "http://practical.dev/api/v1/classes/get-list-for-grade",
                params: {'grade_id': $scope.grade.grade},
                headers: {
                    'Pyxle-auth-token': $scope.randomNumberGenerator(),
                },
            }).then(function mySucces(response) {

                $scope.isSubmitting = false;
                if (response && response.status && response.status === 200) {
                    $scope.classList = response.data.data;
                    $scope.classText = '* ' + $scope.classList.length + ' classes found for the selected grade';
                }
            }, function myError(response) {
                $scope.isSubmitting = true;
            });
        };

        /**
         * Create new student
         *
         * @param isValid
         * @param returnType
         */
        $scope.createStudent = function (isValid, returnType) {
            if(isValid){
                $scope.isSubmitting = true;

                $http({
                    method: "POST",
                    url: "http://practical.dev/api/v1/students",
                    data: {
                        first_name: $scope.grade.first_name,
                        last_name: $scope.grade.last_name,
                        surname: $scope.grade.surname,
                        class_id: $scope.grade.class,
                        gender: $scope.grade.gender
                    },
                    headers: {
                        'Pyxle-auth-token': $scope.randomNumberGenerator(),
                    },
                }).then(function mySucces(response) {

                    $scope.isSubmitting = false;
                    if (response && response.status && response.status === 200) {
                        alert('New student added successfully.');
                        window.location = 'students.php';
                    } else {
                        alert(response.data.message);
                    }

                }, function myError(response) {
                    $scope.isSubmitting = false;
                    alert('Unexpected error occurred. Please try again later.');
                });
            }
        };

    });