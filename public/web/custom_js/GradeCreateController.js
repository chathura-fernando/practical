/**
 * @file  GradeCreateController.js
 * GradeCreateController Controller
 * @author  Chathura Fernando
 */

var app = angular.module('myApp', []);

app.controller('GradeCreateController',
    function ($scope, $http, $location) {


        $scope.isLoading = true;
        $scope.isSubmitting = false;
        $scope.errorMessage = '';
        $scope.grade = {};
        $scope.successRedirect = false;
        $scope.helpText = '';

        /**
         * Generate 8 digit random number of authenticate
         *
         * @returns {string}
         */
        $scope.randomNumberGenerator = function () {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
            var string_length = 8;
            var myrnd = [], pos;
            while (string_length--) {
                pos = Math.floor(Math.random() * chars.length);
                myrnd.push(chars.substr(pos, 1));
            }
            return myrnd.join('');
        };

        /**
         * Create new grade
         *
         * @param isValid
         * @param returnType
         */
        $scope.createGrade = function (isValid, returnType) {
            console.log(isValid);
            if(isValid){
                $scope.isSubmitting = true;

                $http({
                    method: "POST",
                    url: "http://practical.dev/api/v1/grades",
                    data: {
                        name: $scope.grade.name,
                        description: $scope.grade.description
                    },
                    headers: {
                        'Pyxle-auth-token': $scope.randomNumberGenerator(),
                    },
                }).then(function mySucces(response) {

                    $scope.isSubmitting = false;
                    if (response && response.status && response.status === 200) {
                        alert('Grade added successfully.');
                        window.location = 'grades.php';
                    } else {
                        alert(response.data.message);
                    }

                }, function myError(response) {
                    $scope.isSubmitting = false;
                    alert('Unexpected error occurred. Please try again later.');
                });
            }
        };

    });