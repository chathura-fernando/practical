<?php include 'templates/header.php'; ?>

        <div id="page-wrapper" ng-app="myApp" ng-controller="StudentCreateController">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Student {{grade.first_name}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!--Response message area-->
            <div class="alert alert-danger" ng-show="!gradeForm.$valid" ng-if="gradeForm.$submitted">
                <ul ng-messages="gradeForm.fist_name.$error" ng-if="gradeForm.$submitted">
                    <li ng-message="required" >First name is required.</li>
                </ul>
                <ul ng-messages="gradeForm.last_name.$error" ng-if="gradeForm.$submitted">
                    <li ng-message="required" >Last name is required.</li>
                </ul>
                <ul  ng-messages="gradeForm.surname.$error" ng-if="gradeForm.$submitted">
                    <li ng-message="required" >Surname is required.</li>
                </ul>
                <ul ng-messages="gradeForm.grade.$error" ng-if="gradeForm.$submitted">
                    <li ng-message="required">Grade is required</li>
                </ul>
                <ul ng-messages="gradeForm.class.$error" ng-if="gradeForm.$submitted">
                    <li ng-message="required">Class is required</li>
                </ul>
            </div>
            <!--Response message area end-->

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            New Student
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <form name="gradeForm" novalidate class="form-horizontal" accept-charset="UTF-8" autocomplete="off">
                                <fieldset ng-disabled="isSubmitting">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel panel-white">
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">First Name<span class="pr-required-ast">*</span></label>
                                                        <div class="col-md-5">
                                                            <input type="text" name="first_name" id="first_name" ng-model="grade.first_name"
                                                                   required class="form-control"/>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">Last Name<span class="pr-required-ast">*</span></label>
                                                        <div class="col-md-5">
                                                            <input type="text" name="last_name" id="last_name" ng-model="grade.last_name"
                                                                   required class="form-control"/>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">Surname<span class="pr-required-ast">*</span></label>
                                                        <div class="col-md-5">
                                                            <input type="text" name="surname" id="surname" ng-model="grade.surname"
                                                                   required class="form-control"/>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">Gender</label>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="gender" ng-model="grade.gender">
                                                                <option value="">Select gender</option>
                                                                <option value="0">Male</option>
                                                                <option value="1">Female</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">Grade<span class="pr-required-ast">*</span></label>
                                                        <div class="col-md-5">
                                                            <select class="form-control" required name="grade" ng-change="getClassList()"
                                                                    ng-model="grade.grade"
                                                                    ng-options="row.id as row.name for row in gradeList">
                                                                <option value="">Select grade</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-md-2 control-label">Class<span class="pr-required-ast">*</span></label>
                                                        <div class="col-md-5">
                                                            <select class="form-control" required name="class"
                                                                    ng-model="grade.class"
                                                                    ng-options="row.id as row.name for row in classList">
                                                                <option value="">Select class</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5" style="color: green;margin-top: 8px;margin-left: -12px;">{{classText}}</div>
                                                    </div>

                                                    <div class="form-group user-top-form-group">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-9 ep-form-buttons pr-dual-for-component dropbutton-create">


                                                            <button class="btn btn-primary bLarge b-create" type="submit"
                                                                    ng-click="createStudent(gradeForm.$valid)" ng-disabled="isSubmitting">
                                                                <i class="fa fa-plus"></i> Create
                                                            </button>
                                                            <a href="students.php">
                                                                <button class="btn btn-success bLarge b-cancel ep-list-view-top-filter-btn" type="button"
                                                                    ><i class="fa fa-times"></i> Cancel
                                                            </button></a>

                                                        </div>
                                                        <div class="col-md-1"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>



                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->


        </div>
        <!-- /#page-wrapper -->
        <script src="custom_js/StudentCreateController.js"></script>

<?php include 'templates/footer.php'; ?>