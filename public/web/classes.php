<?php include 'templates/header.php'; ?>

        <div id="page-wrapper" ng-app="myApp" ng-controller="ClassListController">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Classes</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="well well-sm">
                        <a href="create-classes.php"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Create Class</button></a>
                    </div>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Class list
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" ng-show="classes.length>0">
                            <div class="dataTable_wrapper">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Grade</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="odd gradeX" ng-repeat="class in classes">

                                        <td>{{class.id}}</td>
                                        <td>{{class.name}}</td>
                                        <td>{{class.grade}}</td>
                                        <td>
                                            <div class="">
                                                <button type="button" class="btn btn-danger btn-edge b-delete"
                                                        ng-click="delete(class.id)"
                                                        title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>


                        <div class="col-lg-4" ng-show="classes.length==0">
                            <br>
                            <div class="well">
                                <h4>No classes found</h4>
                                </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">
                        Showing {{paginationText.from}} to {{paginationText.to}} of {{paginationText.total}} entries
                    </div>
                </div>


                <div class="col-sm-6">

                    <!--Pagination-->
                    <div class="panel panel-white">
                        <div class="panel-body ep-panel-body">
                            <form class="ep-pagination-form">
                                    <button type="button" class="btn btn-info" ng-disabled="!isPreviousPage" style="float: left"
                                            ng-click="loadPreviousResult()"
                                            title="Previous"><i
                                            class="fa fa-chevron-left"></i></button>
                                    <button type="button" class="btn btn-info" ng-disabled="!isNextPage" style="float: right"
                                        ng-click="loadNextResult()"
                                        title="Next {{totalPages}}"><i
                                        class="fa fa-chevron-right"></i></button>
                                    <div class="input-group m-b-sm">
                                        <span class="input-group-addon" id="basic-addon1">Page </span>
                                        <input type="number" class="form-control ep-pagination-input"
                                               ng-model="params.page" ng-change="jumpToPage()"
                                               ng-model-options='{ debounce: 1200 }'>
                                        <span class="input-group-addon" id="basic-addon2"> of {{totalPages}}</span>
                                    </div>

                            </form>
                        </div>
                    </div>



                </div>
            </div>


        </div>
        <!-- /#page-wrapper -->
        <script src="custom_js/ClassListController.js"></script>

<?php include 'templates/footer.php'; ?>