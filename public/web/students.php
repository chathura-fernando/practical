<?php include 'templates/header.php'; ?>

        <div id="page-wrapper" ng-app="myApp" ng-controller="StudentListController">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Students</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="well well-sm">
                        <a href="create-students.php"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Create Student</button></a>
                    </div>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Student list
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" ng-show="students.length>0">
                            <div class="dataTable_wrapper">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Class</th>
                                        <th>Grade</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="odd gradeX" ng-repeat="student in students">

                                        <td>{{student.id}}</td>
                                        <td>{{student.first_name}}</td>
                                        <td>{{student.last_name}}</td>
                                        <td>{{student.class}}</td>
                                        <td>{{student.grade}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>


                        <div class="col-lg-4" ng-show="students.length==0">
                            <br>
                            <div class="well">
                                <h4>No Students found</h4>
                                </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">
                        Showing {{paginationText.from}} to {{paginationText.to}} of {{paginationText.total}} entries
                    </div>
                </div>


                <div class="col-sm-6">

                    <!--Pagination-->
                    <div class="panel panel-white">
                        <div class="panel-body ep-panel-body">
                            <form class="ep-pagination-form">
                                    <button type="button" class="btn btn-info" ng-disabled="!isPreviousPage" style="float: left"
                                            ng-click="loadPreviousResult()"
                                            title="Previous"><i
                                            class="fa fa-chevron-left"></i></button>
                                    <button type="button" class="btn btn-info" ng-disabled="!isNextPage" style="float: right"
                                        ng-click="loadNextResult()"
                                        title="Next {{totalPages}}"><i
                                        class="fa fa-chevron-right"></i></button>
                                    <div class="input-group m-b-sm">
                                        <span class="input-group-addon" id="basic-addon1">Page </span>
                                        <input type="number" class="form-control ep-pagination-input"
                                               ng-model="params.page" ng-change="jumpToPage()"
                                               ng-model-options='{ debounce: 1200 }'>
                                        <span class="input-group-addon" id="basic-addon2"> of {{totalPages}}</span>
                                    </div>

                            </form>
                        </div>
                    </div>



                </div>
            </div>


        </div>
        <!-- /#page-wrapper -->
        <script src="custom_js/StudentListController.js"></script>

<?php include 'templates/footer.php'; ?>